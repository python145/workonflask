from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, EqualTo

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Запомни меня сук')
    submit = SubmitField('Войти')

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('confirm',message="Повторите пароль")])
    confirm = PasswordField('Повторите пароль')
    remember_me = BooleanField('Запомни меня сук')
    submit = SubmitField('Регистрация')
    